package com.example.arcitecture

import android.app.Application
import com.example.arcitecture.data.dataBase.UserDataBase
import com.example.arcitecture.data.repository.UserRepository

class UserApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initialize()
    }

    private fun initialize() {
        val dataBase = UserDataBase.getDatabase(applicationContext)
        val userRepository = UserRepository(dataBase, applicationContext)
    }
}