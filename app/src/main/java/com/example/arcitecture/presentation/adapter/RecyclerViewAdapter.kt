package com.example.arcitecture.presentation.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.arcitecture.R
import com.example.arcitecture.domain.entities.UserData1
import com.example.arcitecture.presentation.viewModel.MainViewModel
import kotlinx.android.synthetic.main.itemrv.view.*

class RecyclerViewAdapter(private val userList: List<UserData1>, private val mainViewModel: MainViewModel)
    : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>()


{
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(data: UserData1) {
            itemView.tv_name.text = data.name
            itemView.tv_email.text = data.emailId
            itemView.tv_phone.text = data.phone
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.itemrv,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data= userList[position]
        holder.itemView.favourite.setOnClickListener {
           // Log.e("hi",data.id.toString())
            data.favourite = true
            mainViewModel.updateUser(data)
//            findNavController().navigate(R.id.action_add2_to_home2)
           // mainViewModel.onlyFav()
            ////Log.e("hi",data.toString())
        }

        holder.itemView.delete.setOnClickListener {
            mainViewModel.delete(data)

        }
        holder.bind(data)

    }

    override fun getItemCount(): Int {
        return userList.size
    }
}