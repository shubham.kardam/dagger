package com.example.arcitecture.presentation.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.arcitecture.R
import com.example.arcitecture.data.dataBase.UserDataBase
import com.example.arcitecture.data.repository.UserRepository
import com.example.arcitecture.databinding.FragmentHomeBinding
import com.example.arcitecture.domain.entities.UserData1
import com.example.arcitecture.presentation.adapter.RecyclerViewAdapter
import com.example.arcitecture.presentation.viewModel.MainViewModel
import com.example.arcitecture.presentation.viewModel.MainViewModelFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Home.newInstance] factory method to
 * create an instance of this fragment.
 */

class Home : Fragment() {


    val uesrList = ArrayList<UserData1>()
    private lateinit var  adapter :  RecyclerViewAdapter
    private lateinit var homeBinding: FragmentHomeBinding
    private lateinit var mainViewModel: MainViewModel
    private lateinit var userRepository: UserRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        Log.e("home","onCreateView")
        homeBinding= FragmentHomeBinding.inflate(inflater)

        userRepository = UserRepository(UserDataBase.getDatabase(requireActivity().applicationContext))
        mainViewModel = ViewModelProvider(this, MainViewModelFactory(userRepository)).get(
            MainViewModel::class.java)












//        mainViewModel.list.observe(requireActivity(), Observer {
//            uesrList.clear()
//            uesrList.addAll(it)
//            Log.e("onViewCreated","$uesrList")
//            adapter.notifyDataSetChanged()
//        })

        homeBinding.btAdd.setOnClickListener {
            findNavController().navigate(R.id.action_home2_to_add2)
        }

        homeBinding.btFavourite.setOnClickListener {
            findNavController().navigate(R.id.action_home2_to_favourite)
        }
        return homeBinding.root
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_home2, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e("home"," onViewCreated")
        mainViewModel.list.observe(requireActivity(), Observer {
            homeBinding.recyclerview.layoutManager = LinearLayoutManager(requireActivity())
            val adapter = RecyclerViewAdapter(it,mainViewModel)

            adapter.notifyDataSetChanged()

            homeBinding.recyclerview.adapter = adapter
        })
    }


}