package com.example.arcitecture.presentation.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.arcitecture.R
import com.example.arcitecture.UserApplication
import com.example.arcitecture.data.dataBase.UserDataBase
import com.example.arcitecture.data.repository.UserRepository
import com.example.arcitecture.databinding.FragmentAddBinding
import com.example.arcitecture.domain.entities.UserData
import com.example.arcitecture.domain.entities.UserData1
import com.example.arcitecture.presentation.viewModel.MainViewModel
import com.example.arcitecture.presentation.viewModel.MainViewModelFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Add.newInstance] factory method to
 * create an instance of this fragment.
 */
class Add : Fragment() {


    private lateinit var mainViewModel: MainViewModel
    private lateinit var addBinding: FragmentAddBinding
    private lateinit var  userRepository:UserRepository
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       // userRepository = (requireAContext.application as UserApplication)

        addBinding = FragmentAddBinding.inflate(inflater)
        userRepository = UserRepository(UserDataBase.getDatabase(requireActivity().applicationContext))
        mainViewModel = ViewModelProvider(this, MainViewModelFactory(userRepository)).get(MainViewModel::class.java)
        addBinding = FragmentAddBinding.inflate(inflater)


        mainViewModel.list.observe(requireActivity(), Observer {

        })

        addBinding.btSubmit.setOnClickListener {
            addUserDetail()
        }

        return addBinding.root
    }


    /**
     *  adding user details
     */
    private fun addUserDetail() {
        mainViewModel.addUserData(
            UserData1(null ,
                addBinding.etName.text.trim().toString(),
                addBinding.etEmail.text.trim().toString(),
                addBinding.etPhone.text.trim().toString(),
                false
            )

        )
        findNavController().navigate(R.id.action_add2_to_home2)
        Toast.makeText(requireActivity(),"Details has been saved",Toast.LENGTH_LONG)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }


}