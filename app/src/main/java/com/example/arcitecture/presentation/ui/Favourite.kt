package com.example.arcitecture.presentation.ui

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.arcitecture.data.dataBase.UserDataBase
import com.example.arcitecture.data.repository.UserRepository
import com.example.arcitecture.databinding.FragmentFavouriteBinding
import com.example.arcitecture.domain.entities.UserData1
import com.example.arcitecture.presentation.adapter.FavouriteRecyclerView
import com.example.arcitecture.presentation.viewModel.MainViewModel
import com.example.arcitecture.presentation.viewModel.MainViewModelFactory



class Favourite : Fragment() {

//    val uesrList = ArrayList<UserData1>()
    private lateinit var favouriteBinding: FragmentFavouriteBinding
    private lateinit var userRepository: UserRepository
    private lateinit var mainViewModel: MainViewModel



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        favouriteBinding = FragmentFavouriteBinding.inflate(inflater)

        userRepository = UserRepository(UserDataBase.getDatabase(requireActivity().applicationContext))
        mainViewModel = ViewModelProvider(this, MainViewModelFactory(userRepository)).get(
            MainViewModel::class.java)

        return favouriteBinding.root

    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainViewModel.list.observe(requireActivity(), Observer {
            val myList = ArrayList<UserData1>()
            val size = it.size -1
            for(i in 0..size)
            {
                if(it[i].favourite == true)
                    myList.add(it[i])
            }
            favouriteBinding.favRecylerview.layoutManager= LinearLayoutManager(context)
            val adapter = FavouriteRecyclerView(myList)
            adapter.notifyDataSetChanged()
            favouriteBinding.favRecylerview.adapter = adapter
        })
    }


}