package com.example.arcitecture.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.arcitecture.R
import com.example.arcitecture.domain.entities.UserData1
import com.example.arcitecture.presentation.viewModel.MainViewModel
import kotlinx.android.synthetic.main.itemrv.view.*

class FavouriteRecyclerView(private val list: List<UserData1>)
    : RecyclerView.Adapter<FavouriteRecyclerView.ViewHolder>()
{
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(data: UserData1) {
            itemView.tv_name.text = data.name
            itemView.tv_email.text = data.emailId
            itemView.tv_phone.text = data.phone
            itemView.favourite.visibility = GONE
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.itemrv2,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = list[position]
        holder.bind(data)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}