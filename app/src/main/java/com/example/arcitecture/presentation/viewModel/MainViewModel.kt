package com.example.arcitecture.presentation.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.arcitecture.data.repository.UserRepository
import com.example.arcitecture.domain.entities.UserData
import com.example.arcitecture.domain.entities.UserData1
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel( private val userRepository: UserRepository)  : ViewModel()
{

//    init {
//        userRepository.getAll()
//    }

    val list : LiveData<List<UserData1>>
        get() {

            return userRepository.users
        }

    val list2 : LiveData<List<UserData1>>
    get() = userRepository.favUser


    fun addUserData(userData1: UserData1)
    {
        viewModelScope.launch(Dispatchers.IO) {
            userRepository.add(userData1)
        }

    }

    fun updateUser(userData1: UserData1)
    {
        viewModelScope.launch(Dispatchers.IO) {
            userRepository.updateUser(userData1)
        }

    }

    fun delete(userData1: UserData1)
    {
        viewModelScope.launch(Dispatchers.IO) {
            userRepository.delete(userData1)
        }
    }
//    fun onlyFav()
//    {
//
//    }



}