package com.example.arcitecture.domain.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "userData1")
data class UserData1(

    @PrimaryKey(autoGenerate = true)
    val id:Int? = null,
    val name:String,
    val emailId:String,
    val phone:String,
    var favourite:Boolean
)
