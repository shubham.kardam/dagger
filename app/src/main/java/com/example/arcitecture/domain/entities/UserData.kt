package com.example.arcitecture.domain.entities

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "Users")
data class UserData (

    @PrimaryKey(autoGenerate = true)
    val id:Int? = null,
    val name:String,
    val emailId:String,
    val phone:String

        )